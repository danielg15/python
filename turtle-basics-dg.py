import turtle
# main function
def main():
	screen = turtle.Screen()
	t = turtle.Turtle()
	screen.bgcolor("magenta")
	#t.tracer(0, 0)
	t.pencolor("#00ffff")#red
	t.begin_fill()
	t.width(10)
	t.fd(100)
	t.lt(45)
	t.pencolor("#00ffff")#green
	t.width(10)
	t.bk(75)
	t.rt(45)
	t.pencolor("#00ffff")#blue
	t.width(10)
	t.goto(0,0)
	t.penup()
	t.end_fill()
	

	#fill Poly Start
	t.goto(-100,200)
	t.pendown()
	t.pencolor("#00ffff")#cyan
	t.penup
	t.begin_fill()
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.end_fill()
	
	t.penup()
	t.goto(100,100)
	t.pendown()
	t.penup()
	t.pencolor("#00ffff")#cyan
	t.pendown()
	t.begin_fill()
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.end_fill()
	
	t.penup()
	t.goto(-55, 150)
	t.pendown()
	t.circle(10)
	t.pencolor("#ffff00")
	t.circle(20)
	t.penup()
	
	
	t.penup()
	t.pencolor("#00ffff")#cyan
	t.goto(150, 150)
	t.pendown()
	t.circle(10)
	t.pencolor("#ffff00")
	t.circle(20)
	t.penup()
	
	
	t.penup()
	t.fillcolor("#00ffff")
	t.goto(180,-110)
	t.pendown()
	t.begin_fill()
	t.lt(19)
	t.fd(90)
	t.rt(20)
	t.fd(90)
	t.rt(20)
	t.fd(80)
	t.rt(159)
	t.fd(250)
	t.end_fill()
	t.penup()
	screen.exitonclick()	
	

if __name__ == "__main__":
		main()
		

