# import required modules
import turtle
import time
import random
  

delay = 0.1
score = 0
high_score = 0
  
#Car
car = turtle.Turtle()
screen = turtle.Screen()
  
# Below code for drawing rectangular upper body
screen.bgcolor("#e20074")
car.color('#74e200')
car.fillcolor('#74e200')
car.penup()
car.goto(-175,-250)####################
car.pendown()
car.begin_fill()
car.forward(370)
car.left(90)
car.forward(50)
car.left(90)
car.forward(370)
car.left(90)
car.forward(50)
car.end_fill()
   
    
# Below code for drawing window and roof
car.penup()
car.goto(-75, -200)####################
car.pendown()
car.setheading(45)
car.forward(70)
car.setheading(0)
car.forward(100)
car.setheading(-45)
car.forward(70)
car.setheading(90)
car.penup()
car.goto(25, -200)#####################
car.pendown()
car.forward(49.50)
   
    
# Below code for drawing two tyres
car.penup()
car.goto(-75, -260)####################
car.pendown()
car.color('#000000')
car.fillcolor('#000000')
car.begin_fill()
car.circle(20)
car.end_fill()
car.penup()
car.goto(150, -260)####################
car.pendown()
car.color('#000000')
car.fillcolor('#000000')
car.begin_fill()
car.circle(20)
car.end_fill()
car.penup()
car.goto(140, -260)####################

def goleft():
    if car.direction != "right":
        car.direction = "left"
  
  
def goright():
    if car.direction != "left":
        car.direction = "right"
        
def move():
    if car.direction == "left":
        x = car.xcor()
        car.setx(x-20)
    if car.direction == "right":
        x = car.xcor()
        car.setx(x+20)
          
screen.listen()
screen.onkeypress(goleft, "b")
screen.onkeypress(goright, "n")


#create a smile face

screen = turtle.Screen()
#t.tracer(0, 0)'
t = turtle.Turtle()
t.penup()
t.width(5)
t.goto(-10,-170)
t.pendown()
t.fd(1)#eye
t.penup()
t.goto(10,-170)
t.pendown()
t.fd(1)
t.penup()
t.goto(-20,-190)
t.lt(-30)#smiley angle
t.pendown()
t.lt(5)
t.circle(50,50)
t.lt(35)
t.penup()
t.goto(140,-260)



  
# Creating a window screen
wn = turtle.Screen()
wn.title("Snake Game")
wn.bgcolor("#e20074")
# the width and height can be put as user's choice
wn.setup(width=600, height=600)
wn.tracer(0)
  
# head of the snake
head = turtle.Turtle()
head.shape("square")
head.color("white")
head.penup()
head.goto(0, 0)
head.direction = "Stop"
  
# food in the game
food = turtle.Turtle()
colors = random.choice(['white', '#6e00e2'])
shapes = random.choice(['square', 'square', 'circle'])
food.speed(0)
food.shape(shapes)
food.color(colors)
food.penup()
food.goto(0, 100)
  
pen = turtle.Turtle()
pen.speed(0)
pen.shape("square")
pen.color("white")
pen.penup()
pen.hideturtle()
pen.goto(0, 250)
pen.write("Score : 0  High Score : 0", align="center",
          font=("candara", 24, "bold"))
  
  
  
# assigning key directions
def goup():
    if head.direction != "down":
        head.direction = "up"
  
  
def godown():
    if head.direction != "up":
        head.direction = "down"
  
  
def goleft():
    if head.direction != "right":
        head.direction = "left"
  
  
def goright():
    if head.direction != "left":
        head.direction = "right"
  
  
def move():
    if head.direction == "up":
        y = head.ycor()
        head.sety(y+20)
    if head.direction == "down":
        y = head.ycor()
        head.sety(y-20)
    if head.direction == "left":
        x = head.xcor()
        head.setx(x-20)
    if head.direction == "right":
        x = head.xcor()
        head.setx(x+20)
  
  
          
wn.listen()
wn.onkeypress(goup, "w")
wn.onkeypress(goup, "i")
wn.onkeypress(godown, "s")
wn.onkeypress(godown, "k")
wn.onkeypress(goleft, "a")
wn.onkeypress(goleft, "j")
wn.onkeypress(goright, "d")
wn.onkeypress(goright, "l")
  
segments = []
  
  
  
# Main Gameplay
while True:
    wn.update()
    if head.xcor() > 290 or head.xcor() < -290 or head.ycor() > 290 or head.ycor() < -290:
        time.sleep(1)
        head.goto(0, 0)
        head.direction = "Stop"
        colors = random.choice(['red', 'blue', 'green'])
        shapes = random.choice(['square', 'circle'])
        for segment in segments:
            segment.goto(1000, 1000)
        segments.clear()
        score = 0
        delay = 0.5
        pen.clear()
        pen.write("Score : {} High Score : {} ".format(
            score, high_score), align="center", font=("candara", 24, "bold"))
    if head.distance(food) < 20:
        x = random.randint(-270, 270)
        y = random.randint(-270, 270)
        food.goto(x, y)
  
        # Adding segment
        new_segment = turtle.Turtle()
        new_segment.speed(0)
        new_segment.shape("square")
        new_segment.color("#0074e2")  # tail colour
        new_segment.penup()
        segments.append(new_segment)
        new_segment = ("5")
        delay -= 0.001
        score += 10
        if score > high_score:
            high_score = score
        pen.clear()
        pen.write("Score : {} High Score : {} ".format(
            score, high_score), align="center", font=("candara", 24, "bold"))
    # Checking for head collisions with body segments
    for index in range(len(segments)-1, 0, -1):
        x = segments[index-1].xcor()
        y = segments[index-1].ycor()
        segments[index].goto(x, y)
    if len(segments) > 0: 
        x = head.xcor() 
        y = head.ycor()
        segments[0].goto(x, y)
    move()
    for segment in segments:
        if segment.distance(head) < 20:
            time.sleep(1)
            head.goto(0, 0)
            head.direction = "stop"
            colors = random.choice(['red', 'blue', 'green'])
            shapes = random.choice(['square', 'circle'])
            for segment in segments:
                segment.goto(1000, 1000)
            segment.clear()
  
            score = 0
            delay = 0.1
            pen.clear()
            pen.write("Score : {} High Score : {} ".format(
                score, high_score), align="center", font=("candara", 24, "bold"))
    time.sleep(delay)
  
wn.mainloop()

if __name__ == "__game__":
		game()
